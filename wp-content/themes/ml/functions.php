<?php
////////////////////////////////////////////////////////////////////////////////
/*******************************************************************************
* 	Theme Setup
*******************************************************************************/
////////////////////////////////////////////////////////////////////////////////
remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
remove_action( 'wp_head', 'index_rel_link' ); // index link
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_filter( 'template_redirect', 'redirect_canonical'); // Remove canonical redirect for make pagination work with single.php
remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); // Remove emoji script
remove_action( 'wp_print_styles', 'print_emoji_styles' ); // Remove emoji style


if(is_admin()){
	add_action('login_enqueue_scripts', add_editor_style(get_template_directory_uri().'/css/editor.css' )); // Edit style
	add_action('admin_enqueue_scripts', wp_enqueue_style('admin-style', get_template_directory_uri().'/css/admin.css' )); // Admin style
	add_filter('admin_footer_text', 'change_footer_admin');
	function change_footer_admin () { // Replace footer text
		_e('Developed by <a href="http://metlaor.com" target="_blank">MetLaor Team</a>');	}
}else{
	if( $pagenow == "wp-login.php"){
		add_action('login_enqueue_scripts', wp_enqueue_style('login-style', get_template_directory_uri().'/css/login.css' )); // Login style
	}	
	
	add_filter('nav_menu_css_class', 'custom_wp_nav_menu');
	add_filter( 'nav_menu_css_class', 'my_special_nav_class', 10, 2 ); // Add current class in single
}





function custom_wp_nav_menu($var) { //Remove Extraneous Classes From Wordpress Mewnus
	return is_array($var) ? array_intersect($var, array(
		// List of useful classes to keep
		'current_page_item',
		'current_page_parent',
		'current-page-ancestor'
		)
	) : '';
}
function my_special_nav_class( $classes, $item ) {
	// If single then check post type label to add current class
    if ( is_single() && $item->title == ucwords(str_replace("-"," ",get_template_name())) ) {
        $classes[] = 'current-page-ancestor';
    }
    return $classes;
}

if(!is_page('contact-us')){
	add_filter( 'wpcf7_load_js', '__return_false' );
	add_filter( 'wpcf7_load_css', '__return_false' );
}

////////////////////////////////////////////////////////////////////////////////
/*******************************************************************************
* 	Get template name
*******************************************************************************/
////////////////////////////////////////////////////////////////////////////////
function get_template_name(){
	global $post;
	$id		  = $post->ID;
	$parents  = $post->post_parent;
	$name	  = $post->post_name;
	
	if(is_single()):
		$name = substr(get_post_type(), 2);
	else:
		if($parents != 0):
			
			// Top-level parent Pages
			$ancestors 	 = get_post_ancestors( $id );
			$ancestorsid = ($ancestors) ? $ancestors[count($ancestors)-1]: $id;
		
			// Name of Top-level parent Pages
			$name = wp_basename(get_permalink($ancestorsid));
			
		endif;
	endif;
	
	return $name;
}


////////////////////////////////////////////////////////////////////////////////
/*******************************************************************************
* 	Post View
*******************************************************************************/
////////////////////////////////////////////////////////////////////////////////
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0";
    }
    return $count;
}


////////////////////////////////////////////////////////////////////////////////
/*******************************************************************************
* 	Register Menu
*******************************************************************************/
//////////////////////////////////////////////////////////////////////////////// 
function register_my_menus() {
	register_nav_menus( array( 'mainMenu' => __( 'Main Menu' ) ) );	
}
add_action( 'init', 'register_my_menus' );


////////////////////////////////////////////////////////////////////////////////
/*******************************************************************************
*	Media Size
*******************************************************************************/
////////////////////////////////////////////////////////////////////////////////
add_image_size( 'project-thumbnail-big', 555, 390, array( 'center', 'center' ) );
add_image_size( 'project-thumbnail-medium', 442, 311, array( 'center', 'center' ) );
add_image_size( 'project-thumbnail-small', 360, 253, array( 'center', 'center' ) );

// More condition
add_filter( 'jpeg_quality', create_function('$quality', 'return 70;' )); // Set quility of image
add_filter( 'image_size_names_choose', 'custom_image_sizes_choose' ); // Add image size to admin
function custom_image_sizes_choose( $sizes ) {  
    $custom_sizes = array(  
        'project-thumbnail-big' => 'Thumbnail Big',
        'project-thumbnail-medium' => 'Thumbnail Medium',
        'project-thumbnail-small' => 'Thumbnail Small',
    );
    return array_merge( $sizes, $custom_sizes ); 
}


////////////////////////////////////////////////////////////////////////////////
/*******************************************************************************
* 	Limit lenght of post title
*******************************************************************************/
////////////////////////////////////////////////////////////////////////////////  		
function shortenText($text, $maxlength = 70, $appendix = "..."){
	$text = strip_tags($text);		
	if(mb_strlen($text) <= $maxlength) { return $text; }
	$text = mb_substr($text, 0, $maxlength - mb_strlen($appendix));
	$text .= $appendix;
	return $text;
}


////////////////////////////////////////////////////////////////////////////////
/*******************************************************************************
* 	Add Colorbox to link cover image
*******************************************************************************/
//////////////////////////////////////////////////////////////////////////////// 
function bizcolorbox($content) {
	global $post;
	
	// For image
	$content = preg_replace("/<a(.*?)href=('|\")(.*?).(bmp|gif|jpeg|jpg|png)('|\")(.*?)>/i", '<a$1href=$2$3.$4$5 class="galleries"$6>', $content);
	
	// For pdf
	$content = preg_replace("/<a(.*?)href=('|\")(.*?).(pdf)('|\")(.*?)>/i", '<a$1href=$2$3.$4$5 class="pdf"$6>', $content);
	
	// Return content
	return $content;
}


////////////////////////////////////////////////////////////////////////////////
/*******************************************************************************
* 	Rewrite Search
*******************************************************************************/
//////////////////////////////////////////////////////////////////////////////// 
function change_search_url_rewrite() {
    if ( is_search() && ! empty( $_GET['s'] ) ) {
        wp_redirect( home_url( "/search/" ) . urlencode( get_query_var( 's' ) ) .'/');
        exit();
    }
}
add_action( 'template_redirect', 'change_search_url_rewrite' );


////////////////////////////////////////////////////////////////////////////////
/*******************************************************************************
*	Pagination
*******************************************************************************/
//////////////////////////////////////////////////////////////////////////////// 
function bizpagination($offset=0){
	global $wp_query;
	
	$URL 	 	= stripslashes(get_pagenum_link($page - 1)).'page/';
	$CURRENT 	= get_query_var('paged') ? get_query_var('paged') : 1;
	$PERPAGE 	= intval(get_query_var('posts_per_page'));	
	$PAGES 	 	= intval(ceil(($wp_query->found_posts - $offset) / $PERPAGE));
	$PAGERAGE	= 5; // 3 or 5
	$GAP		= '<li>&bull;&bull;&bull;</li>';
	
	if($PAGES > 1){			
		if($PAGES <= $PAGERAGE){			
			$START	= 1;
			$LOOP 	= $PAGES;
		}else{
			if($CURRENT < $PAGERAGE){
				$START	= 1;
				$LOOP	= $PAGERAGE;
				$RGAP	= $GAP;
			}else{
				if($PAGES > ($CURRENT + ceil($PAGERAGE / 2))){
					$START	= $CURRENT - floor($PAGERAGE / 2);
					$LOOP	= $CURRENT + floor($PAGERAGE / 2);
					$RGAP	= $GAP;
					if($CURRENT != 3){
						$LGAP = $GAP;
					}
				}elseif($PAGES == ($CURRENT + ceil($PAGERAGE / 2))){
					$START	= $CURRENT - floor($PAGERAGE / 2);
					$LOOP	= $CURRENT + floor($PAGERAGE / 2);
					$LGAP	= $GAP;
				}else{
					$START	= $PAGES - (ceil($PAGERAGE / 2) + 1);
					$LOOP	= $PAGES - 1;
					$LGAP	= $GAP;
				}
				
				// First		
				if($CURRENT > 1){
					$FIRST 	= '<li><a href="'.$URL.'1">01</a></li>';
				}else{
					$FIRST 	= '<li><a href="'.$URL.'1" class="disabled">01</a></li>';
				}
			}	
			
			// Last	
			if($PAGES<10){ $PAGES = "0".$PAGES; }
			else{ $PAGES = $PAGES; }
				
			if($CURRENT < $PAGES){
				$LAST 	= '<li><a href="'.$URL.$PAGES.'">'.$PAGES.'</a></li>';
			}else{
				$LAST	= '<li><a href="'.$URL.$PAGES.'" class="current disabled">'.$PAGES.'</a></li>';
			}	
		}
		
		// Prev
		if($CURRENT > 1){
			$PREV 	= '<li><a href="'.$URL.($CURRENT - 1).'" class="prev"></a></li>';
		}else{
			$PREV 	= '<li><a href="'.$URL.'1" class="prev-disabled"></a></li>';
		}
		
		// Next
		if($CURRENT < $PAGES){
			$NEXT 	= '<li><a href="'.$URL.($CURRENT + 1).'" class="next"></a></li>';
		}else{
			$NEXT 	= '<li><a href="'.$URL.$PAGES.'" class="next-disabled"></a></li>';
		}	
		
		for($k=$START; $k<=$LOOP; $k++){
			
			if($k<10){ $label = "0".$k; }
			else{ $label = $k; }
			
			if($START==$k){
				if($CURRENT==$k){
					$PAGE .= '<li><a href="'.$URL.$k.'" class="current">'.$label.'</a></li>';
				}else{
					$PAGE .= '<li><a href="'.$URL.$k.'">'.$label.'</a></li>';
				}
			}else{
				if($CURRENT==$k){
					$PAGE .= '<li><a href="'.$URL.$k.'" class="current">'.$label.'</a></li>';
				}else{
					$PAGE .= '<li><a href="'.$URL.$k.'">'.$label.'</a></li>';
				}
			}
		}
		
		wp_reset_query();
		if(!is_page('home')){
			
			return'
				<div class="container" style="text-align: center;">
					<div class="pagination">
						<ul>'.$PREV.$FIRST.$LGAP.$PAGE.$RGAP.$LAST.$NEXT.'</ul>
					</div>
				</div>
			';
		}
	}
}