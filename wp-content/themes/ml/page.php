<?php
/*
 *	Template Name: Page
 */	
 	
// Header
get_header();						

// Get Content Template
get_template_part( 'templates/desktop/content', get_template_name() ); 

// Footer              
get_footer(); 
?>