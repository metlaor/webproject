<?php 
/*
 *	Template Name: Single
 */
 
// Header
get_header();						

// Get Content Template
get_template_part( 'templates/desktop/content-single', get_template_name() ); 

// Footer              
get_footer(); 
?>