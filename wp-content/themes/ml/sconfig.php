<link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url')?>/style.css"/>
<link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url')?>/css/template-<?php _e(get_template_name()); ?>.css"/>


<script type="text/javascript" src="<?php bloginfo('template_url')?>/scripts/jquery.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url')?>/scripts/bootstrap.min.js"></script>
<script type="text/javascript">
	
	jQuery(window).load(function() {
		$("body").removeAttr("class");
	});
	
</script>